//API -- https://jsonplaceholder.typicode.com/

//Нужно найти в документации jsonplaceholder где лежат тудушки
// const rawResponse = await fetch(/**/);

//Нужно привести сырой ответ сервера в формат JSON
//const todos = await response.???();

/**
 * Функция ответственная за рендеринг ответа с сервера
 */
const renderedTodos = todos.map((todo) => {
  /**
   Ваш код
   ! Обратите внимание, что если JSONPlaceholder отдает завершенную
   тудушку, то мы должны помечать это через атрибут checked для элемента input
  */
});

//Элемент, к которому нужно добавить отрендеренные тудушки
//К этому элементу в дальнейшем будет применена функция createElement
const app = {
  tag: 'div',
  child: [],
};

app.child = [renderedTodos];

export default app;
